import numpy as np
import math
from typing import TypeVar, Iterable, Tuple, List, Dict


Predictable = TypeVar ('Predictable', float, Iterable, np.ndarray)

def average(x,y):
    return (x*1.0 + y*1.0) / 2.0

def decisionTree(x):
    if x==0:
        return 0
    else:
        return math.log2(x)

class classifierErrors(Exception):
    pass

def sortPredicitions(predictors: np.ndarray, targets: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    assert predictors.shape[0] == targets.shape[0] # Return True or False
    sorted_indices = np.argsort(predictors)
    return predictors[sorted_indices], targets[sorted_indices]

def deltaInfo(info: np.ndarray) -> List[int]:
    delta = []
    for i in range(1, info.shape[0]):
        if info[i] != info[i - 1]:
            delta.append(i)
    return delta
def splitData(split: np.ndarray, i: int) -> Tuple[np.ndarray, np.ndarray]:
    return (
        split[0:i], split[i:len(split)]
    )

def classifierData(classInfo: np.ndarray) -> Dict:
    data = {}
    keys, values = np.unique(classInfo, return_counts=True)
    for key, value in zip(keys, values):
        data[key] = value
    return data
def maxStump(stumpInfoma: np.ndarray) -> int:
    classes, counts = np.unique(stumpInfoma, return_counts=True)
    maxArgs = np.argmax(counts)
    return classes[maxArgs]

class decisionStump:
    def __init__(self):
        self.stumpDecision = None
        self.info, self._targets = [None] * 2
        self.current, self.next = [None] * 2
        self.stumpData = 1.0

    @property
    def decisions(self) -> float:
        return self.stumpDecision

    @property
    def information(self) -> float:
        return self.stumpData

    def sortData(self, predictors: np.ndarray, targets: np.ndarray) -> None:
        self.info = np.copy(predictors)
        self._targets = np.copy(targets)
        self.info, self._targets = sortPredicitions(self.info, self._targets)
        self.split_set(self.info, self._targets)

    def predict(self, testInfo: Predictable) -> np.ndarray:
        try:
            _ = iter(testInfo)

        except TypeError:
            testInfo = [testInfo]
        return np.array(
            [self.findValues(predictor) for predictor in testInfo]
        )

    def findValues(self, i: float) -> int:
        value = None

        if i <= self.decisions:
            value = self.current
        else:
            value = self.next
        return value

    def split_set(self, predictors: np.ndarray, targets: np.ndarray) -> None:
        deltaValue = deltaInfo(targets)
        if len(deltaValue) == 0:
            raise classifierErrors()
        i, dat = -1, -1
        for index in deltaValue:
            currData, nextData = splitData(targets, index)
            info = self.set_info(currData, nextData)
        if info >= dat:
            i = index
            dat = info
        self.modelTree(i, dat)

    def set_info(self, currentData: np.ndarray, nextData: np.ndarray) -> float:
        total = len(self._targets)
        currLength, nextLength = len(currentData), len(nextData)
        curr, n = currLength / total, nextLength / total
        e = self.ensemble_method(self._targets)
        return (
                e - (curr * self.ensemble_method(currentData) + n * self.ensemble_method(nextData))
        )

    def ensemble_method(self, data: np.ndarray) -> float:
        sigmaValue = 0
        total = len(data)
        for target, target_count in classifierData(data).items():
            p = target_count / total
            sigmaValue += -(p * decisionTree(p))
        return sigmaValue

    def modelTree(self, index: int, data: float) -> None:
        self.stumpDecision = average(self.info[index - 1], self.info[index])
        self.stumpData = data
        currData, nextData = splitData(self._targets, index)
        self.current = maxStump(currData)
        self.next = maxStump(nextData)

    def __repr__(self):
        def printArray(a):
            return [str(x) for x in a]

        return 'Decision Stump: {}\nValue Predictions: {}\nTarget Values: {}'.format(
            self.decisions,
            ' | '.join(printArray(self.info)),
            ' | '.join(printArray(self._targets))
        )

class AdaBoost:

    def __init__(self, adaboostRounds=10):
        self.info, self._targets, self.index = [None] * 3
        self.adaboostRounds = adaboostRounds
        self.ensembleMethod = []
        self.alphas = []

    @staticmethod
    def probabilities(sampleSet): # Return Probabilities
        weightDistribution = 1 / sampleSet
        return np.array([weightDistribution] * sampleSet)

    def sortData(self, predictors: np.ndarray, targets: np.ndarray, verbose=True) -> None:
        self.setData(predictors, targets)

        weights = AdaBoost.probabilities(len(self._targets))
        boost = 0
        while boost < self.adaboostRounds:
            sample_predictors, sample_targets = self.sampleDataset(weights)  # Sample Data Using Weig
            stump = decisionStump()
            try:  # Train classifier sets
                stump.sortData(sample_predictors, sample_targets)
            except classifierErrors:
                weights = AdaBoost.probabilities(len(self._targets))
                continue
            predictions = stump.predict(self.info)
            invalid = self.invalidPredictions(predictions)
            weightErrors = self.weightedError(invalid, weights)

            if weightErrors >= .5:
                weights = self.probabilities(len(self._targets))
                continue
            else:
                boost += 1
                alpha = .5 * math.log((1 - weightErrors) / weightErrors)
                self._add_model(stump, alpha)
                if verbose:
                    def print_div():

                        print('_______________________________________________')

                    def printArray(a):
                        return [str(x) for x in a]

                    print_div()
                    print(
                        'Alphas: {}\nError Rate: {}'.format(
                            alpha, weightErrors
                        )
                    )
                    print("")
                    print('Weight Distributions:')
                    print(
                        ' | '.join(printArray(weights))
                    )
                    print("")
                    print(stump)
                    print_div()
                    print("")
                weights = self.newWeightDistribution(weights, invalid, alpha)

    def predict(self, x):
        try:
            _ = iter(x)
        except TypeError:
            x = [x]
        predictions = []
        for value in x:
            predictions.append(self.determineSigma(value))
        return np.array(predictions)

    def determineSigma(self, s):
        sigmas = 0
        for alpha, model in zip(self.alphas, self.ensembleMethod):
            sigmas += (alpha * model.predict(s)[0])
        if sigmas < 0:
            return -1
        else:
            return 1

    def setData(self, predictors: np.ndarray, targets: np.ndarray) -> None:
        self.info = np.copy(predictors)
        self._targets = np.copy(targets)
        self.index = list(range(len(targets)))

    def sampleDataset(self, probabilities: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
        obtainIndex = np.random.choice(self.index, size=len(self._targets), replace=True, p=probabilities)
        return self.info[obtainIndex], self._targets[obtainIndex]

    def invalidPredictions(self, x):
        return self._targets != x

    @staticmethod
    def weightedError(invalidWeights: np.ndarray, weights: np.ndarray):
        x = invalidWeights.astype(np.int)
        return np.dot(weights, x)

    def _add_model(self, model, alpha):
        self.ensembleMethod.append(model)
        self.alphas.append(alpha)

    def newWeightDistribution(self, weights, invalid, alpha):
        newDistribution = []
        for isInvalid, weight in zip(invalid, weights):
            if isInvalid:
                x = -alpha
        else:
            x = alpha
        newDistribution.append(weight * math.exp(x))
        newDistribution = np.array(newDistribution)
        newDistribution /= newDistribution.sum()
        return newDistribution
if __name__ == '__main__':
    testData = np.array([.5, 3.0, 4.5, 4.6, 4.9, 5.2, 5.3, 5.5, 7.0, 9.5])
    targetValues = np.array([-1, -1, 1, 1, 1, -1, -1, 1, -1, -1])
    adaBoost = AdaBoost(10)
    adaBoost.sortData(testData, targetValues, verbose=True)
    testVals = np.arange(1, 11) * 1.0
    print('Testing Data:')
    print(testVals)
    print('Test Value Predictions:')
    print(adaBoost.predict(testVals))